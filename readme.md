![npm](https://img.shields.io/npm/v/ramster-ui-core.svg)
<br/>
ramster-ui-core
==
The ramster-ui-core module provides a set of components and utilities for building application front-end using Angular 9. Part of the ramster open-source software toolkit.
