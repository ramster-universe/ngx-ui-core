import {AppStateService, IAppStateServiceRedirectOptions} from '../index'
import {OnInit} from '@angular/core'
import {Router} from '@angular/router'


export class BaseLayoutComponent implements OnInit {
	loaderActive: boolean = false

	constructor(
		public appStateService: AppStateService,
		public router: Router
	) {
	}

	ngOnInit(): void {
		this.appStateService.pageLoadedObservable.subscribe(() => this.pageLoaded())
		this.appStateService.triggerInitialDataLoadObservable.subscribe(() => this.loadInitialData())
		this.appStateService.redirectObservable.subscribe(({route, options}) => this.redirect(route, options))
		this.appStateService.toggleLoaderObservable.subscribe((active) => this.toggleLoader(active))
	}

	sendInitialDataLoadedEvent() {
		this.appStateService.initialDataLoaded()
	}


	/*
	 * appStateService handlers start
	 */

	loadInitialData(): void {
		this.sendInitialDataLoadedEvent()
	}

	pageLoaded(): void {
		if (this.appStateService.state.initialDataLoaded) {
			this.sendInitialDataLoadedEvent()
			return
		}
		this.loadInitialData()
	}

	redirect(route: string, options?: IAppStateServiceRedirectOptions): void {
		const actualOptions = options || {},
			{queryParams, reloadInitialData} = actualOptions
		if (reloadInitialData) {
			this.appStateService.setStateData({initialDataLoaded: false}, {triggerStateChanged: false})
		}
		this.router.navigate([route], {queryParams: queryParams || {}})
	}

	toggleLoader(active: boolean): void {
		this.loaderActive = active
	}

	/*
	 * appStateService handlers end
	 */
}
