import {Directive, ViewContainerRef} from '@angular/core'

@Directive({
	selector: '[rui-component-injector-area]'
})
export class ComponentInjectorAreaDirective {
	constructor(public viewContainerRef: ViewContainerRef) {
	}
}
