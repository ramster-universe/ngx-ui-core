import {ActivatedRoute} from '@angular/router'
import {AppStateService} from '../index'
import {OnDestroy, OnInit} from '@angular/core'
import {Subject} from 'rxjs'
import {takeUntil} from 'rxjs/operators'

export class BasePageComponent implements OnInit, OnDestroy {
	destroyed: Subject<void> = new Subject()

	constructor(
		public activatedRoute: ActivatedRoute,
		public appStateService: AppStateService,
		public onInitMethodNames: string[],
		public onInitialDataLoadedMethodNames: string[]
	) {
	}

	ngOnInit() {
		const instance = this as any
		this.appStateService.initialDataLoadedObservable.pipe(takeUntil(this.destroyed)).subscribe(() => this.initialDataLoaded())
		this.onInitMethodNames.forEach((methodName) => {
			if (typeof instance[methodName] === 'function') {
				instance[methodName]()
			}
		})
	}

	ngOnDestroy() {
		this.destructor()
	}

	destructor() {
		this.destroyed.next()
		this.destroyed.complete()
	}

	reset(): void {
		// this.queryParams = this.activatedRoute.snapshot.queryParams
		// this.routeParams = this.activatedRoute.snapshot.params
	}

	sendPageLoadedEvent() {
		// this.appStateService.pageLoaded({queryParams: this.queryParams, routeParams: this.routeParams})
		this.appStateService.pageLoaded()
	}

	initialDataLoaded(): void {
		const instance = this as any
		this.onInitialDataLoadedMethodNames.forEach((methodName) => {
			if (typeof instance[methodName] === 'function') {
				instance[methodName]()
			}
		})
	}
}
