import {AppStateService, RequestService} from '../../index'
import {CustomError, emptyToNull, stringifyNestedObjects} from '@ramster/general-tools'
import {HttpHeaders, HttpEvent, HttpEventType, HttpResponse} from '@angular/common/http'
import {IHandleErrorOptions, IMethodOptions} from './baseREST.interfaces'
import {Injectable} from '@angular/core'
import {Subject} from 'rxjs'

@Injectable()
export class BaseRESTService {
	baseUrl = '/'
	fileTooLargeErrorMessage: string = 'The selected file is too large.'
	headers = new HttpHeaders({'Content-Type': 'application/json'})
	redirectOnForbiddenUrl: string | null = null

	constructor(
		public appStateService: AppStateService,
		public requestService: RequestService
	) {}

	handleError(err: HttpResponse<any>, options?: IHandleErrorOptions): void {
		const {notifyOnError} = options || {} as IHandleErrorOptions
		if (!err) {
			if (notifyOnError !== false) {
				this.appStateService.notify('error', 'An unknown error has occurred.')
			}
			return
		}
		if (this.redirectOnForbiddenUrl && ((err.status === 401) || (err.body.error && (err.body.error.status === 401)))) {
			this.appStateService.redirect(this.redirectOnForbiddenUrl)
			return
		}
		if ((err.status === 413) || (err.body.error && (err.body.error.status === 413))) {
			this.appStateService.notify('error', this.fileTooLargeErrorMessage)
			return
		}
		if (notifyOnError !== false) {
			this.appStateService.notify('error', err.body.error && err.body.error.error || 'An error has occurred.')
		}
	}

	create(data: {[fieldName: string]: any}, options?: IMethodOptions): Promise<any> {
		return new Promise((resolve, reject) => {
			const {errorHandlingOptions} = options || {} as IMethodOptions
			this.requestService.run(
				'post',
				this.baseUrl,
				{
					data: emptyToNull(data),
					headers: this.headers,
					observe: 'response'
				}
			).subscribe((event: HttpEvent<any>) => {
				if (event.type !== HttpEventType.Response) {
					reject(new CustomError(`Invalid HttpEvent type "${event.type}", expected "${HttpEventType.Response}".`))
				}
				event = event as HttpResponse<any>
				if (event.status > 399) {
					this.handleError(event, errorHandlingOptions)
					reject({error: true})
				}
				resolve(event.body)
			})
		})
	}

	read(data: {[fieldName: string]: any}, options?: IMethodOptions): Promise<any> {
		return new Promise((resolve, reject) => {
			const {errorHandlingOptions} = options || {} as IMethodOptions
			this.requestService.run(
				'get',
				`${this.baseUrl}/item`,
				{
					data: stringifyNestedObjects(emptyToNull(data) as any),
					headers: this.headers,
					observe: 'response'
				}
			).subscribe((event: HttpEvent<any>) => {
				if (event.type !== HttpEventType.Response) {
					reject(new CustomError(`Invalid HttpEvent type "${event.type}", expected "${HttpEventType.Response}".`))
				}
				event = event as HttpResponse<any>
				if (event.status > 399) {
					this.handleError(event, errorHandlingOptions)
					reject({error: true})
				}
				resolve(event.body)
			})
		})
	}

	readList(data: {[fieldName: string]: any}, options?: IMethodOptions): Promise<any> {
		return new Promise((resolve, reject) => {
			const {errorHandlingOptions} = options || {} as IMethodOptions
			this.requestService.run(
				'get',
				this.baseUrl,
				{
					data: stringifyNestedObjects(emptyToNull(data) as any),
					headers: this.headers,
					observe: 'response'
				}
			).subscribe((event: HttpEvent<any>) => {
				if (event.type !== HttpEventType.Response) {
					reject(new CustomError(`Invalid HttpEvent type "${event.type}", expected "${HttpEventType.Response}".`))
				}
				event = event as HttpResponse<any>
				if (event.status > 399) {
					this.handleError(event, errorHandlingOptions)
					reject({error: true})
				}
				resolve(event.body)
			})
		})
	}

	streamReadList(
		data: {[key: string]: any},
		onMessage: Function,
		options?: {onError?: Function, reconnectAttemptInterval?: number, reconnectAttemptsLeft?: number}
	): Subject<void> {
		const actualOptions = options || {},
			{onError, reconnectAttemptInterval} = actualOptions,
			errorHandler = onError ? onError : this.handleError.bind(this)
		let reconnectAttemptsLeft = actualOptions.reconnectAttemptsLeft,
			url = `${window.location.origin}${this.baseUrl}/streamList`,
			stringifiedParams = stringifyNestedObjects(emptyToNull(data) as any),
			firstParam = true
		for (const key in stringifiedParams) {
			if (firstParam) {
				firstParam = false
				url += '?'
			} else {
				url += '&'
			}
			url += `${key}=${stringifiedParams[key]}`
		}
		let eventSource = new EventSource(url),
			closeSubject = new Subject<void>(),
			reconnectAllowed = {value: true}
		eventSource.onmessage = (event) => onMessage(event)
		eventSource.onerror = (err) => errorHandler(err)
		let interval = setInterval(() => {
			if (
				(eventSource.readyState === 2) &&
				reconnectAllowed.value &&
				((typeof reconnectAttemptsLeft === 'undefined') || reconnectAttemptsLeft > 0)
			) {
				eventSource = new EventSource(url)
				eventSource.onmessage = (event) => onMessage(event)
				eventSource.onerror = (err) => errorHandler(err)
				if (typeof reconnectAttemptsLeft !== 'undefined') {
					reconnectAttemptsLeft--
				}
				return
			}
			clearInterval(interval)
		}, reconnectAttemptInterval || 5000)
		closeSubject.subscribe(() => {
			reconnectAllowed.value = false
			eventSource.close()
		})
		return closeSubject
	}

	readSelectList(data: {[fieldName: string]: any}, options?: IMethodOptions): Promise<any> {
		return new Promise((resolve, reject) => {
			const {errorHandlingOptions} = options || {} as IMethodOptions
			this.requestService.run(
				'get',
				`${this.baseUrl}/selectList`,
				{
					data: stringifyNestedObjects(emptyToNull(data) as any),
					headers: this.headers,
					observe: 'response'
				}
			).subscribe((event: HttpEvent<any>) => {
				if (event.type !== HttpEventType.Response) {
					reject(new CustomError(`Invalid HttpEvent type "${event.type}", expected "${HttpEventType.Response}".`))
				}
				event = event as HttpResponse<any>
				if (event.status > 399) {
					this.handleError(event, errorHandlingOptions)
					reject({error: true})
				}
				resolve(event.body)
			})
		})
	}

	update(id: number | number[], data: {[fieldName: string]: any}, options?: IMethodOptions): Promise<any> {
		return new Promise((resolve, reject) => {
			const {errorHandlingOptions} = options || {} as IMethodOptions
			this.requestService.run(
				'patch',
				`${this.baseUrl}/item/${id instanceof Array ? id.join(',') : id}`,
				{
					data: emptyToNull(data),
					headers: this.headers,
					observe: 'response'
				}
			).subscribe((event: HttpEvent<any>) => {
				if (event.type !== HttpEventType.Response) {
					reject(new CustomError(`Invalid HttpEvent type "${event.type}", expected "${HttpEventType.Response}".`))
				}
				event = event as HttpResponse<any>
				if (event.status > 399) {
					this.handleError(event, errorHandlingOptions)
					reject({error: true})
				}
				resolve(event.body)
			})
		})
	}

	bulkUpsert(data: {[fieldName: string]: any}, options?: IMethodOptions): Promise<any> {
		return new Promise((resolve, reject) => {
			const {errorHandlingOptions} = options || {} as IMethodOptions
			this.requestService.run(
				'put',
				this.baseUrl,
				{
					data: emptyToNull(data),
					headers: this.headers,
					observe: 'response'
				}
			).subscribe((event: HttpEvent<any>) => {
				if (event.type !== HttpEventType.Response) {
					reject(new CustomError(`Invalid HttpEvent type "${event.type}", expected "${HttpEventType.Response}".`))
				}
				event = event as HttpResponse<any>
				if (event.status > 399) {
					this.handleError(event, errorHandlingOptions)
					reject({error: true})
				}
				resolve(event.body)
			})
		})
	}

	delete(id: number | number[], data: {[fieldName: string]: any}, options?: IMethodOptions): Promise<any> {
		return new Promise((resolve, reject) => {
			const {errorHandlingOptions} = options || {} as IMethodOptions
			this.requestService.run(
				'delete',
				`${this.baseUrl}/${id instanceof Array ? id.join(',') : id}`,
				{
					data: emptyToNull(data),
					headers: this.headers,
					observe: 'response'
				}
			).subscribe((event: HttpEvent<any>) => {
				if (event.type !== HttpEventType.Response) {
					reject(new CustomError(`Invalid HttpEvent type "${event.type}", expected "${HttpEventType.Response}".`))
				}
				event = event as HttpResponse<any>
				if (event.status > 399) {
					this.handleError(event, errorHandlingOptions)
					reject({error: true})
				}
				resolve(event.body)
			})
		})
	}
}
