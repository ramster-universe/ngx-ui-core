export interface IMethodOptions {
	errorHandlingOptions?: IHandleErrorOptions
}

export interface IHandleErrorOptions {
	notifyOnError?: boolean
}
