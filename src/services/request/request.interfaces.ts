export interface IRequestRunOptions {
	data?: any
	[fieldName: string]: any
}
