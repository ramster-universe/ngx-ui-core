import {flattenObject} from '@ramster/general-tools'
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http'
import {Injectable} from '@angular/core'
import {IRequestRunOptions} from './request.interfaces'
import {Observable} from 'rxjs'


@Injectable()
export class RequestService {
	constructor(public client: HttpClient) {}

	run(method: string, url: string, options?: IRequestRunOptions): Observable<HttpEvent<any>> {
		const {data, ...requestOptions} = options || {}
		let actualUrl = url,
			body = null
		if (method.toLowerCase() === 'get') {
			const optionsParams = flattenObject(data || {})
			actualUrl += `?_=${(new Date()).getTime().toString()}`
			if (optionsParams.length) {
				optionsParams.forEach((item) => {
					actualUrl += `&${item.key}=${item.value}`
				})
			}
		} else {
			body = (!data || (typeof data !== 'object')) ? {} : data
			body._ = (new Date()).getTime()
		}
		return this.client.request(new HttpRequest(method, actualUrl, body, requestOptions))
	}
}
