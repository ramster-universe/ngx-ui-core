import {fromJS} from 'immutable'
import {
	IAppStateServiceRedirectOptions,
	IAppStateServiceStateChangedData,
	IAppStateServiceSetStateDataOptions
} from './appState.interfaces'
import {Injectable} from '@angular/core'
import {setNested} from '@ramster/general-tools'
import {Subject} from 'rxjs'

@Injectable({
	providedIn: 'root'
})
export class AppStateService {
	state: {
		initialDataLoaded: boolean
		loggedInUser: any
		queryParams: {[x: string]: string}
		routeParams: {[x: string]: string}
		[fieldName: string]: any
	} = {
		initialDataLoaded: false,
		loggedInUser: null,
		queryParams: {},
		routeParams: {}
	}


	initialDataLoadedSubject = new Subject<any>()
	initialDataLoadedObservable = this.initialDataLoadedSubject.asObservable()
	initialDataLoaded(): void {
		this.initialDataLoadedSubject.next()
	}

	notifySubject = new Subject<{type: string, message: string}>()
	notifyObservable = this.notifySubject.asObservable()
	notify(type: string, message: string): void {
		this.notifySubject.next({type, message})
	}

	pageLoadedSubject = new Subject<{[fieldName: string]: any} | void>()
	pageLoadedObservable = this.pageLoadedSubject.asObservable()
	pageLoaded(layoutData?: {[fieldName: string]: any}): void {
		this.pageLoadedSubject.next(layoutData)
	}

	redirectSubject = new Subject<{route: string, options?: IAppStateServiceRedirectOptions}>()
	redirectObservable = this.redirectSubject.asObservable()
	redirect(route: string, options?: IAppStateServiceRedirectOptions): void {
		this.redirectSubject.next({route, options})
	}

	setLayoutDataSubject = new Subject<{[fieldName: string]: any}>()
	setLayoutDataObservable = this.setLayoutDataSubject.asObservable()
	setLayoutData(data?: {[fieldName: string]: any}): void {
		this.setLayoutDataSubject.next(data)
	}

	setStateData(data: {[fieldName: string]: any}, options?: IAppStateServiceSetStateDataOptions): void {
		const {triggerStateChanged} = options || {} as IAppStateServiceSetStateDataOptions,
			previousState = fromJS(this.state).toJS()
		let changedKeys = []
		for (const key in data) {
			setNested(this, key, data[key])
			changedKeys.push(key)
		}
		if (triggerStateChanged !== false) {
			this.stateChanged({changedKeys, newState: this.state, previousState})
		}
	}

	stateChangedSubject = new Subject<IAppStateServiceStateChangedData>()
	stateChangedObservable = this.stateChangedSubject.asObservable()
	stateChanged(data: IAppStateServiceStateChangedData): void {
		this.stateChangedSubject.next(data)
	}

	toggleLoaderSubject = new Subject<boolean>()
	toggleLoaderObservable = this.toggleLoaderSubject.asObservable()
	toggleLoader(active: boolean): void {
		this.toggleLoaderSubject.next(active)
	}

	triggerInitialDataLoadSubject = new Subject<void>()
	triggerInitialDataLoadObservable = this.triggerInitialDataLoadSubject.asObservable()
	triggerInitialDataLoad(): void {
		this.triggerInitialDataLoadSubject.next()
	}
}
