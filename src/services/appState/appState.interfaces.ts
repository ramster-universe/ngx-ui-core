export interface IAppStateServiceRedirectOptions {
	reloadInitialData?: boolean,
	queryParams?: {[x: string]: string}
}

export interface IAppStateServiceStateChangedData {
	changedKeys: string[]
	newState: {[fieldName: string]: any}
	previousState: {[fieldName: string]: any}
}

export interface IAppStateServiceSetStateDataOptions {
	triggerStateChanged?: boolean
}
