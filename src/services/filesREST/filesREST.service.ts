import {AppStateService, BaseRESTService, RequestService} from '../../index'
import {CustomError} from '@ramster/general-tools'
import {HttpEvent, HttpEventType, HttpHeaders, HttpResponse} from '@angular/common/http'
import {Injectable} from '@angular/core'
import {IUploadMethodOptions} from './filesREST.interfaces'

@Injectable()
export class FilesRESTService extends BaseRESTService {
	baseUrl = '/files'

	constructor(
		appStateService: AppStateService,
		requestService: RequestService
	) {
		super(appStateService, requestService)
	}

	upload(file: File, params: {outputFileName: string, [x: string]: any}, options?: IUploadMethodOptions): Promise<any> {
		return new Promise((resolve, reject) => {
			const {errorHandlingOptions, handleError} = options || {} as IUploadMethodOptions
			let fd = new FormData()
			fd.append('file', file, file.name)
			for (const key in params) {
				fd.append(key, params[key])
			}
			let headers = new HttpHeaders()
			headers.set('Content-Type', 'multipart/form-data')
			this.requestService.run(
				'post',
				`${this.baseUrl}`,
				{
					data: fd,
					headers,
					observe: 'response'
				}
			).subscribe((event: HttpEvent<any>) => {
				if (event.type !== HttpEventType.Response) {
					reject(new CustomError(`Invalid HttpEvent type "${event.type}", expected "${HttpEventType.Response}".`))
				}
				event = event as HttpResponse<any>
				if (event.status > 399) {
					if (handleError) {
						this.handleError(event, errorHandlingOptions)
					}
					reject({error: true})
				}
				resolve(event.body)
			})
		})
	}
}
