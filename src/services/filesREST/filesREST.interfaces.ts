import {IMethodOptions} from '../../index'
export interface IUploadMethodOptions extends IMethodOptions {
	handleError?: boolean
}
