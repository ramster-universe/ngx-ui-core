import {CommonModule} from '@angular/common'
import {ComponentInjector} from './components/componentInjector/componentInjector.component'
import {ComponentInjectorAreaDirective} from './components/componentInjector/componentInjector.directive'
import {NgModule} from '@angular/core'

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		ComponentInjector,
		ComponentInjectorAreaDirective
	],
	exports: [
		ComponentInjector
	]
})
export class RamsterUICoreModule {}

export * from './components/base-layout.component'
export * from './components/base-page.component'
export * from './services/baseREST'
export * from './services/filesREST'
export * from './services/appState/appState.service'
export * from './services/appState/appState.interfaces'
export * from './services/modelRESTServiceProvider.service'
export * from './services/request'
export * from './interfaces/selectList.interface'
